import java.util.Scanner;
public class Gravitacija {
    
    public static double gravitacijskiPospesek(int v){
        
        double C=6.674*Math.pow(10,(-11));
        double r=6.371*Math.pow(10,6);
        double M=5.972*Math.pow(10,24);
        
        return (C*M)/Math.pow(r+v,2);
    }
    public static void main(String [] args) {
        Scanner sc = new Scanner(System.in);
        int nadmorska = sc.nextInt();
        evoGravitacijksiPospesek(nadmorska, gravitacijskiPospesek(nadmorska));
    }
    
    public static void evoGravitacijksiPospesek (int v, double g ){
        
        System.out.println(v);
        System.out.println(g);
        
    }
    
}